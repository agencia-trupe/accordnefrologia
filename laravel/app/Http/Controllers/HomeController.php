<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\CadastrosRequest;

use App\Models\SobreOPrograma;
use App\Models\Palestrante;
use App\Models\Aula;
use App\Models\AulasAviso;

class HomeController extends Controller
{
    public function index()
    {
        if (auth('cadastro')->check()) {
           return redirect()->route('sobre');
        }

        return redirect()->route('loginCadastro');
    }

    public function sobre()
    {
        $sobre = SobreOPrograma::first();

        return view('frontend.sobre', compact('sobre'));
    }

    public function aulas()
    {
        $aviso = AulasAviso::first()->aviso;
        // $aulas = Aula::with('palestrante')->ordenados()->get();

        return view('frontend.aulas', compact('aviso'));
    }

    public function aula(Aula $aula)
    {
        if (! $aula->isAtiva) abort('404');

        $user = auth('cadastro')->user();
        $aula = $aula->adicionaProgresso($user);
        $duvidas = $aula->duvidas()->with('cadastro')
            ->respondidas()->ordenados()->get();

        return view('frontend.aula', compact('user', 'aula', 'duvidas'));
    }

    public function aulaDuvidas(Aula $aula)
    {
        if (! $aula->isAtiva) abort('404');

        $duvidas = $aula->duvidas()->with('cadastro')
            ->respondidas()->ordenados()->paginate(10);

        return view('frontend.aula-duvidas', compact('aula', 'duvidas'));
    }

    public function aulaAssistida($id)
    {
        if (! request()->ajax()) {
            return response('Acesso não autorizado.', 401);
        }

        $aula = Aula::findOrFail($id);

        if (! $aula->isAtiva) abort('404');

        $user = auth('cadastro')->user();

        if (! $user->aulas->contains($id)) {
            $user->aulas()->attach($id);
        }
    }

    public function aulaQuestaoPost(Request $request, $aulaId, $questaoId)
    {
        if (! request()->ajax()) {
            return response('Acesso não autorizado.', 401);
        }

        $this->validate($request, [
            'alternativa' => 'required|integer'
        ]);

        $aula        = Aula::findOrFail($aulaId);
        $questao     = $aula->questoes()->findOrFail($questaoId);
        $questao->load('alternativas');
        $alternativa = $questao->alternativas()->findOrFail(request('alternativa'));

        if (! $aula->isAtiva) abort('404');

        $user = auth('cadastro')->user();

        if ($user->respostaQuestao($questao->id)) {
            return response()->json([
                'erro' => 'Erro: Essa questão já foi respondida.'
            ]);
        }

        $user->respostas()->create([
            'questao_id'     => $questao->id,
            'alternativa_id' => $alternativa->id
        ]);

        $correta = $questao->alternativas()
            ->where('id', $alternativa->id)
            ->where('alternativa_correta', true)
            ->count();

        return response()->json([
            'selecao'  => (int)request('alternativa'),
            'correta'  => (bool)$correta
        ]);
    }

    public function aulaDuvidaPost(Request $request, $aulaId)
    {
        if (! request()->ajax()) {
            return response('Acesso não autorizado.', 401);
        }

        $this->validate($request, [
            'duvida' => 'required'
        ]);

        $aula = Aula::findOrFail($aulaId);
        $user = auth('cadastro')->user();

        if (! $aula->isAtiva) abort('404');

        $aula->duvidas()->create([
            'cadastro_id' => $user->id,
            'duvida'      => request('duvida')
        ]);

        return response('', 200);
    }

    public function palestrantes()
    {
        $palestrantes = Palestrante::ordenados()->get();

        return view('frontend.palestrantes', compact('palestrantes'));
    }

    public function dados()
    {
        $user = auth('cadastro')->user();

        return view('frontend.dados-cadastrais', compact('user'));
    }

    public function dadosPost(CadastrosRequest $request)
    {
        $input = array_filter($request->except('senha_confirmation'), 'strlen');

        if (isset($input['senha'])) {
            $input['senha'] = bcrypt($input['senha']);
        }

        auth('cadastro')->user()->update($input);

        return redirect()->back()->with('success', 'Cadastro alterado com sucesso!');
    }
}
