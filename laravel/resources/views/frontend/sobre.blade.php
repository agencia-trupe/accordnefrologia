@extends('frontend.common.template')

@section('content')

<div class="main sobre">
    <div class="center">
        @if($sobre->texto != "")
        <div class="texto">
            <h1>{!! $sobre->titulo !!}</h1>
            {!! $sobre->texto !!}
        </div>
        @else
        <div class="texto">
            <iframe class="video" src="{{ 'https://www.youtube.com/embed/jCKf3O4ucR0' }}"></iframe>
        </div>
        @endif
    </div>
</div>

@endsection