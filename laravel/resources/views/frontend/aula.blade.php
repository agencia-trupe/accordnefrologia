@extends('frontend.common.template')

@section('content')

<div class="main texto aulas">
    <div class="center">
        @if($aula->titulo)
        <h1>{{ $aula->titulo }}</h1>
        @else
        <h1>AULAS</h1>
        @endif

        @if($aula->video)
        <div class="video-wrapper" data-concluir="{{ route('aulas.assistida', $aula->id) }}">
            <iframe src="{{ $aula->video }}" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen scrolling="no" style="overflow: hidden"></iframe>
        </div>
        @endif

        <div class="aula">
            <div class="aula-forum">
                <h3>FÓRUM</h3>
                <p>Envie suas dúvidas para o especialista.</p>
                <form action="" id="form-duvida" data-url="{{ route('aulas.duvidaPost', $aula->id) }}">
                    <textarea name="duvida" required></textarea>
                    <input type="submit" value="ENVIAR">
                    <div id="form-duvida-response"></div>
                </form>
                @if(count($duvidas))
                <div class="duvidas">
                    @foreach($duvidas->take(6) as $duvida)
                    <div class="duvida">
                        <span class="dados">
                            {{ $duvida->cadastro->nome }} &middot;
                            {{ $duvida->created_at->format('d/m/Y | H:i\h') }}
                        </span>
                        <p>{!! nl2br($duvida->duvida) !!}</p>
                        <p class="resposta">{!! nl2br($duvida->resposta) !!}</p>
                    </div>
                    @endforeach

                    @if(count($duvidas) > 6)
                    <a href="{{ route('aulas.duvidas', $aula->slug) }}">
                        <span>VER MAIS</span>
                    </a>
                    @endif
                </div>
                @endif
            </div>
        </div>
    </div>
</div>

@endsection