<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        setlocale(LC_TIME, 'pt_BR');

        view()->composer('frontend.home', function($view) {
            $view->with('apresentacao', \App\Models\SobreOPrograma::first()->apresentacao);
        });

        view()->composer('frontend.common.template', function($view) {
            $view->with('config', \App\Models\Configuracoes::first());
            $view->with('contato', \App\Models\Contato::first());
            $view->with('aulaNefrologia', \App\Models\Aula::where('data_liberacao', '<=', date("Y-m-d"))->first());
        });

        view()->composer('painel.common.nav', function($view) {
            $view->with('contatosNaoLidos', \App\Models\ContatoRecebido::naoLidos()->count());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
