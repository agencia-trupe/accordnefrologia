<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AulaQuestao extends Model
{
    protected $table = 'aulas_questoes';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'ASC');
    }

    public function aula()
    {
        return $this->belongsTo(Aula::class, 'aula_id');
    }

    public function alternativas()
    {
        return $this->hasMany(AulaQuestaoAlternativa::class, 'questao_id')->ordenados();
    }
}
