<?php

namespace App\Http\Controllers;

use App\Models\Aula;

class CertificadoController extends Controller
{
    public function index()
    {
        $user = auth('cadastro')->user();

        $aulas = Aula::with('questoes')->has('questoes')->ordenados()->get();
        $aulas = $aulas->map(function($aula) use ($user) {
            return $aula->adicionaProgresso($user);
        });

        return view('frontend.certificado', compact('aulas'));
    }

    public function emissao(Aula $aula)
    {
        $user = auth('cadastro')->user();

        $aula = $aula->adicionaProgresso($user);

        if (! $aula->respondida || ! $aula->aptaAoCertificado) {
            return response('Você não atingiu os requisitos para emissão deste certificado.', 400);
        }

        if (! $aula->certificado) {
            return response('Ocorreu um erro ao emitir o certificado. Entre em contato para mais informações.', 500);
        }

        $dompdf = new \Dompdf\Dompdf();
        $dompdf->loadHtml(view(
            'frontend.certificado-pdf', compact('user', 'aula')
        )->render());
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream("AccordAcademy_Certificado-Modulo{$aula->modulo}.pdf");
    }
}
