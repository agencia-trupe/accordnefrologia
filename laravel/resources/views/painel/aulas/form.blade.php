@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('data_liberacao', 'Data de liberação') !!}
    {!! Form::text('data_liberacao', null, ['class' => 'form-control datepicker']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('video', 'Código do Vídeo') !!}
    {!! Form::text('video', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.aulas.index') }}" class="btn btn-default btn-voltar">Voltar</a>
