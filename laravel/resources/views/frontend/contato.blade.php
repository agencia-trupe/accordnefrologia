@extends('frontend.common.template')

@section('content')

    <div class="main texto contato">
        <div class="center">
            <div class="contato-informacoes">
                <p class="telefone">{{ $contato->telefone }}</p>
                <p class="endereco">{!! $contato->endereco !!}</p>
            </div>

            <div class="texto-conteudo">
                <h1>CONTATO</h1>

                <form action="{{ route('contato.post') }}" method="POST" class="form-padrao">
                    {!! csrf_field() !!}

                    @if($errors->any())
                    <div class="erro">
                        Preencha todos os campos corretamente
                    </div>
                    @endif
                    @if(session('enviado'))
                    <div class="sucesso">
                        Mensagem enviada com sucesso!
                    </div>
                    @endif

                    <div class="row">
                        <label for="nome">NOME</label>
                        <input type="text" name="nome" id="nome" value="{{ old('nome') }}" required>
                    </div>
                    <div class="row">
                        <label for="email">E-MAIL</label>
                        <input type="email" name="email" id="email" value="{{ old('email') }}" required>
                    </div>
                    <div class="row">
                        <label for="telefone">TELEFONE</label>
                        <input type="text" name="telefone" id="telefone" value="{{ old('telefone') }}">
                    </div>
                    <div class="row">
                        <label for="mensagem">MENSAGEM</label>
                        <textarea name="mensagem" id="mensagem" required>{{ old('mensagem') }}</textarea>
                    </div>

                    <input type="submit" value="ENVIAR">
                </form>
            </div>
        </div>
    </div>

@endsection
