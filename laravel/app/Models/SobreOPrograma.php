<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class SobreOPrograma extends Model
{
    protected $table = 'sobre_o_programa';

    protected $guarded = ['id'];

}
