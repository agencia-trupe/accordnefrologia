<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use Illuminate\Support\Facades\Cache;

class Aula extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'aulas';

    protected $guarded = ['id'];

    public static function boot()
    {
        parent::boot();

        self::created(function($model) {
            Cache::forget('aulas_count');
            return static::cachedCount();
        });

        self::deleted(function($model) {
            Cache::forget('aulas_count');
            return static::cachedCount();
        });
    }

    public static function cachedCount()
    {
        return Cache::rememberForever('aulas_count', function () {
            return static::count();
        });
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('modulo', 'ASC')->orderBy('id', 'DESC');
    }

    public function setDataLiberacaoAttribute($date)
    {
        $this->attributes['data_liberacao'] = Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
    }

    public function getDataLiberacaoAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }

    public function getIsAtivaAttribute()
    {
        $dataLiberacao = Carbon::createFromFormat('d/m/Y', $this->data_liberacao)
            ->startOfDay();

        return Carbon::now()->gte($dataLiberacao);
    }

    public function palestrante()
    {
        return $this->belongsTo(Palestrante::class, 'palestrante_id');
    }

    public function questoes()
    {
        return $this->hasMany(AulaQuestao::class, 'aula_id')->with('alternativas')->ordenados();
    }

    public function duvidas()
    {
        return $this->hasMany(AulaDuvida::class, 'aula_id')->ordenados();
    }

    public static function upload_moderadora_foto()
    {
        return CropImage::make('moderadora_foto', [
            'width'  => 130,
            'height' => 130,
            'path'   => 'assets/img/moderadoras/'
        ]);
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 375,
            'height' => 210,
            'path'   => 'assets/img/aulas/'
        ]);
    }

    public static function upload_certificado()
    {
        return CropImage::make('certificado', [
            'width'  => null,
            'height' => null,
            'path'   => 'assets/img/certificados/'
        ]);
    }

    public function adicionaProgresso($user)
    {
        $totalQuestoes = $this->questoes->count();

        $respostas = $user->respostas()
            ->with('alternativa')
            ->whereHas('questao.aula', function($q) {
                $q->where('id', $this->id);
            })->get();

        $totalRespondido   = $respostas->count();
        $respostasCorretas = $respostas->filter(function($resposta) {
            return $resposta->alternativa->alternativa_correta;
        })->count();

        if ($totalQuestoes > 0) {
            $respondida = $totalRespondido == $totalQuestoes;
            $porcentagemDeAcertos = round(($respostasCorretas / $totalQuestoes) * 100);
            $aptaAoCertificado = $porcentagemDeAcertos >= 75;
        } else {
            $respondida = false;
            $aptaAoCertificado = false;
        }

        $questionarioBloqueado = false;
        if ($respondida && ! $aptaAoCertificado) {
            $ultimaRespostaData = $respostas->sortByDesc('created_at')->first()->created_at;
            $horaAtual = Carbon::now();

            if ($horaAtual->lt($ultimaRespostaData->addHours(72))) {
                $questionarioBloqueado = true;
            } else {
                $respostas->each(function($resposta) {
                    $resposta->delete();
                });
                $respondida = false;
            }
        }

        $this->respondida = $respondida;
        $this->aptaAoCertificado = $aptaAoCertificado;
        $this->questionarioBloqueado = $questionarioBloqueado;

        return $this;
    }
}
