@extends('frontend.common.template')

@section('content')

<div class="main aulas">
    <div class="center">
        <h1>AULAS</h1>

        @if($aviso)
        <div class="aulas-aviso">
            <p>{{ $aviso }}</p>
        </div>
        @endif

        <div class="texto-antes-evento">
            A transmissão do evento ao vivo se iniciará às 19h30 no dia 1 de dezembro de 2021. Aguardamos você para participar conosco.
            </br>Acesse <a href="http://accordacademy.com.br/">accordacademy.com.br</a> e faça login na área de Nefrologia com os dados recém cadastrados.
            </br>Gratos.
        </div>
    </div>
</div>

@endsection