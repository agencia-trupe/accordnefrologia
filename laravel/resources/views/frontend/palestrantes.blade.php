@extends('frontend.common.template')

@section('content')

    <div class="main palestrantes">
        <div class="center">
            @if(count($palestrantes))
            <h1>COORDENADORA DO PROGRAMA</h1>
            <div class="wrapper">
                @foreach([$palestrantes->shift()] as $palestrante)
                <div class="palestrante">
                    <div class="palestrante-foto">
                        <img src="{{ asset('assets/img/palestrantes/'.$palestrante->foto) }}" alt="">
                    </div>
                    <div class="palestrante-texto">
                        <h3>{{ $palestrante->nome }}</h3>
                        <p>{!! $palestrante->apresentacao !!}</p>
                    </div>
                </div>
                @endforeach
            </div>

            <h1>PALESTRANTES</h1>
            <div class="wrapper">
                @foreach($palestrantes as $palestrante)
                <div class="palestrante">
                    <div class="palestrante-foto">
                        <img src="{{ asset('assets/img/palestrantes/'.$palestrante->foto) }}" alt="">
                    </div>
                    <div class="palestrante-texto">
                        <h3>{{ $palestrante->nome }}</h3>
                        <p>{!! $palestrante->apresentacao !!}</p>
                    </div>
                </div>
                @endforeach
            </div>
            @endif
        </div>
    </div>

@endsection
