<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    // Páginas restritas
    Route::group(['middleware' => ['auth.cadastro']], function() {
        Route::get('sobre', 'HomeController@sobre')->name('sobre');
        Route::get('dados-cadastrais', 'HomeController@dados')->name('dados');
        Route::patch('dados-cadastrais', 'HomeController@dadosPost')->name('dadosPost');
        Route::get('aulas', 'HomeController@aulas')->name('aulas');
        Route::get('aulas/{aula_slug}', 'HomeController@aula')->name('aulas.show');
        Route::get('aulas/{aula_slug}/duvidas', 'HomeController@aulaDuvidas')->name('aulas.duvidas');
        Route::get('aulas/{aula_id}/assistida', 'HomeController@aulaAssistida')->name('aulas.assistida');
        Route::post('aulas/{aula_id}/questao/{questao_id}', 'HomeController@aulaQuestaoPost')->name('aulas.questaoPost');
        Route::post('aulas/{aula_id}/duvida', 'HomeController@aulaDuvidaPost')->name('aulas.duvidaPost');
        // Route::get('palestrantes', 'HomeController@palestrantes')->name('palestrantes');
        // Route::get('certificado', 'CertificadoController@index')->name('certificado');
        // Route::get('certificado/{aula_slug}', 'CertificadoController@emissao')->name('certificado.emissao');
    });

    // Login/Cadastro
    Route::get('login', 'CadastroAuth\AuthController@showLoginForm')->name('loginCadastro');
    Route::post('login', 'CadastroAuth\AuthController@login')->name('loginCadastroPost');
    Route::get('cadastro', 'CadastroAuth\AuthController@showRegistrationForm')->name('cadastro');
    Route::post('cadastro', 'CadastroAuth\AuthController@create')->name('cadastroPost');

    // Redefinição de Senha
    Route::get('esqueci-minha-senha', 'CadastroAuth\PasswordController@showLinkRequestForm')->name('esqueci');
    Route::post('esqueci-minha-senha', 'CadastroAuth\PasswordController@sendResetLinkEmail')->name('redefinicao');
    Route::get('redefinicao-de-senha/{token}', 'CadastroAuth\PasswordController@showResetForm')->name('redefinirForm');
    Route::post('redefinicao-de-senha', 'CadastroAuth\PasswordController@reset')->name('redefinir');

    // Logout
    Route::get('logout', 'CadastroAuth\AuthController@logout')->name('logoutCadastro');


    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
        Route::get('relatorio', 'RelatorioController@index')->name('painel.relatorio.index');
        Route::get('relatorio/{aulas}', 'RelatorioController@show')->name('painel.relatorio.show');
        Route::get('cadastros/exportar', 'CadastrosController@exportar')->name('painel.cadastros.exportar');
        Route::resource('cadastros', 'CadastrosController');
        Route::get('aulas/{aulas}/previa', 'AulasController@previa')->name('painel.aulas.previa');
        Route::put('aulas/aviso', 'AulasController@updateAviso')->name('painel.aulas.aviso');
        Route::get('aulas/{aulas}/teste-certificado', 'AulasController@certificado')->name('painel.aulas.certificado');
        Route::resource('aulas', 'AulasController');
        Route::resource('aulas.questoes', 'AulasQuestoesController');
        Route::resource('aulas.duvidas', 'AulasDuvidasController', ['except' => ['show', 'create', 'store']]);
		Route::resource('aulas.questoes.alternativas', 'AulasQuestoesAlternativasController');
		Route::resource('palestrantes', 'PalestrantesController');
		Route::resource('sobre-o-programa', 'SobreOProgramaController', ['only' => ['index', 'update']]);
        Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);

        // Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        // Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
