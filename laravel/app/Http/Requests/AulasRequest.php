<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AulasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'data_liberacao' => 'required',
            'palestrante_id' => '',
            'moderadora' => '',
            'moderadora_foto' => '',
            'titulo' => '',
            'modulo' => '',
            'carga_horaria' => '',
            'descricao' => '',
            'capa' => '',
            'video' => 'required',
            'certificado' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'palestrante_id' => 'palestrante'
        ];
    }
}
