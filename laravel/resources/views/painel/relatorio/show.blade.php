@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.relatorio.index') }}" title="Voltar para Relatório" class="btn btn-sm btn-default">
        &larr; Voltar para Relatório
    </a>

    <legend>
        <h2>
            <small>Relatório /</small>
            {{ $aula->titulo }}
        </h2>
    </legend>


    @if(!count($users))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="palestrantes">
        <thead>
            <tr>
                <th>Usuários aptos ao certificado</th>
            </tr>
        </thead>

        <tbody>
        @foreach ($users as $user)
            <tr>
                <td>
                    <a href="{{ route('painel.cadastros.show', $user->id) }}">
                        {{ $user->nome }}
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
