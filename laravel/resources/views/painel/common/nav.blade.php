<ul class="nav navbar-nav">
    <li @if(Tools::routeIs('painel.sobre-o-programa*')) class="active" @endif>
        <a href="{{ route('painel.sobre-o-programa.index') }}">Sobre o Programa</a>
    </li>
    <li @if(Tools::routeIs('painel.aulas*')) class="active" @endif>
        <a href="{{ route('painel.aulas.index') }}">Aulas</a>
    </li>
    <li @if(Tools::routeIs('painel.contato.index')) class="active" @endif>
        <a href="{{ route('painel.contato.index') }}">Informações de Contato</a>
    </li>
    <li @if(Tools::routeIs('painel.cadastros*')) class="active" @endif>
        <a href="{{ route('painel.cadastros.index') }}">Cadastros</a>
    </li>
</ul>
