import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

if ($('.aulas .video-wrapper').length) {
    try {
        var iframe = document.querySelector('iframe');
        var player = new Vimeo.Player(iframe);

        var urlConcluir = $('.aulas .video-wrapper').data('concluir');

        player.on('play', function() {
            $.ajax({ url: urlConcluir });
        });
    } catch(err) {
        console.log(err);
    }
}

if ($('.aula-quiz').length) {
    $('.aula-questao button').click(function() {
        var $botao   = $(this),
            $questao = $botao.parent(),
            url      = $questao.data('route'),
            selecao  = parseInt($questao.find('input:checked').val());

        if ($botao.hasClass('disabled')
            || $questao.hasClass('respondida')
            || ! selecao) return;

        $botao.addClass('disabled');

        $.post(url, {
            alternativa: selecao
        }, (data) => {
            if (data.erro) {
                alert(data.erro);
                return;
            }

            $questao.addClass('respondida');

            $questao.find('label').each((index, el) => {
                let $alternativa = $(el).find('input');
                let alternativaId = parseInt($alternativa.val());

                $alternativa.attr('disabled', true);

                if (alternativaId == selecao) {
                    if (data.correta) {
                        $(el).addClass('correta');
                    } else {
                        $(el).addClass('selecao');
                    }
                }
            });

            if (data.correta) {
                $botao.addClass('certo').html('VOCÊ ACERTOU');
            } else {
                $botao.addClass('errado').html('VOCÊ ERROU');
            }
        }).fail(() => {
            alert('Ocorreu um erro. Tente novamente.');
            $botao.removeClass('disabled');
        });
    });
}

$('.nav-scroll').click(function (event) {
    event.preventDefault();

    const id = $(this).attr('href');

    $('html,body').animate({
        scrollTop: $(id).offset().top,
    }, 'slow');
});

if ($('.login').hasClass('scrollToLogin')) {
    $('html,body').animate({
        scrollTop: $('#login').offset().top,
    }, 'slow');
}

$(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if (scroll >= 10) {
        $('.header-fixed').addClass('sticky');
    } else {
        $('.header-fixed').removeClass('sticky');
    }
});

$('#form-duvida').submit(function (event) {
    event.preventDefault();

    const $form = $(this);
    const $response = $('#form-duvida-response');

    if ($form.hasClass('sending')) return false;

    $form.addClass('sending');
    $response.hide();

    $.ajax({
        type: 'POST',
        url: $form.data('url'),
        data: {
            duvida: $form.find('textarea[name=duvida]').val(),
        },
    }).success(() => {
        $response.text('Dúvida enviada com sucesso!');
        $form[0].reset();
    }).fail(() => {
        $response.text('Ocorreu um erro. Tente novamente.');
    }).always(() => {
        $response.fadeIn();
        $form.removeClass('sending');
    });
});

if ($('.nav-scroll').length) {
    const sectionsPositions = $('section').map(function() {
        return $(this).position().top;
    }).toArray();

    const navLinks = $('a.nav-scroll');

    $(window).scroll(function() {
        const distanceFromTop = $(this).scrollTop();
        let current;

        sectionsPositions.forEach((pos, idx) => {
            if (distanceFromTop >= pos - 100) {
                current = navLinks[idx];
            }

            navLinks.removeClass('active');
            $(current).addClass('active');
        });
    }).trigger('scroll');
}
