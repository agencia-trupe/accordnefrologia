@extends('frontend.common.template')

@section('content')

    <div class="main">
        <div class="center">
            <section id="login" class="login">
                <h2>ENTRE OU CADASTRE-SE</h2>
                <h3>Você precisa estar logado para acessar o conteúdo.</h3>
                <div class="wrapper">
                    <form action="{{ route('loginCadastroPost') }}" class="form-login" method="POST">
                        @if(session('erro-login'))
                            <div class="erro">{{ session('erro-login') }}</div>
                        @endif

                        {!! csrf_field() !!}

                        <div class="form-login-content">
                            <div class="fields">
                                <input type="email" name="email" value="{{ old('email') }}" placeholder="e-mail" required>
                                <input type="password" name="senha" placeholder="senha" required>
                            </div>
                            <input type="submit" value="ENTRAR">
                        </div>

                        <a href="{{ route('esqueci') }}" class="btn-esqueci">esqueci minha senha &raquo;</a>
                    </form>

                    <a href="{{ route('cadastro') }}" class="btn-cadastro">
                        AINDA NÃO TENHO CADASTRO > CADASTRAR
                    </a>
                </div>
            </section>
        </div>
    </div>

@endsection
