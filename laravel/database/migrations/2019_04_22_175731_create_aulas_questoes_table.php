<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAulasQuestoesTable extends Migration
{
    public function up()
    {
        Schema::create('aulas_questoes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->integer('aula_id')->unsigned()->nullable();
            $table->foreign('aula_id')->references('id')->on('aulas')->onDelete('cascade');
            $table->text('questao');
            $table->timestamps();
        });

        Schema::create('aulas_questoes_alternativas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->integer('questao_id')->unsigned()->nullable();
            $table->foreign('questao_id')->references('id')->on('aulas_questoes')->onDelete('cascade');
            $table->text('alternativa');
            $table->boolean('alternativa_correta')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('aulas_questoes_alternativas');
        Schema::drop('aulas_questoes');
    }
}
